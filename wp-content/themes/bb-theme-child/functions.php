<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';


add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
	wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);

});
// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 
//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );


add_filter('gform_form_args', 'no_ajax_on_all_forms', 10, 1);
function no_ajax_on_all_forms($args){
    $args['ajax'] = false;
    return $args;
}

function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js", array(), '3.2.1' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );
function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}


// IP location FUnctionality
  if (!wp_next_scheduled('cde_preferred_location_cronjob')) {
    
    wp_schedule_event( time() +  17800, 'daily', 'cde_preferred_location_cronjob');
}

add_action( 'cde_preferred_location_cronjob', 'cde_preferred_location' );

function cde_preferred_location(){

          global $wpdb;

          if ( ! function_exists( 'post_exists' ) ) {
              require_once( ABSPATH . 'wp-admin/includes/post.php' );
          }

          //CALL Authentication API:
          $apiObj = new APICaller;
          $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
          $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);


          if(isset($result['error'])){
              $msg =$result['error'];                
              $_SESSION['error'] = $msg;
              $_SESSION["error_desc"] =$result['error_description'];
              
          }
          else if(isset($result['access_token'])){

              //API Call for getting website INFO
              $inputs = array();
              $headers = array('authorization'=>"bearer ".$result['access_token']);
              $website = $apiObj->call(BASEURL.get_option('SITE_CODE'),"GET",$inputs,$headers);

             // write_log($website['result']['locations']);

              for($i=0;$i<count($website['result']['locations']);$i++){

                  if($website['result']['locations'][$i]['type'] == 'store'){

                      $location_name = isset($website['result']['locations'][$i]['city'])?$website['result']['locations'][$i]['name']:"";

                      $found_post = post_exists($location_name,'','','store-locations');

                          if( $found_post == 0 ){

                                  $array = array(
                                      'post_title' => $location_name,
                                      'post_type' => 'store-locations',
                                      'post_content'  => "",
                                      'post_status'   => 'publish',
                                      'post_author'   => 0,
                                  );
                                  $post_id = wp_insert_post( $array );

                                //  write_log( $location_name.'---'.$post_id);
                                  
                                  update_post_meta($post_id, 'address', $website['result']['locations'][$i]['address']); 
                                  update_post_meta($post_id, 'city', $website['result']['locations'][$i]['city']); 
                                  update_post_meta($post_id, 'state', $website['result']['locations'][$i]['state']); 
                                  update_post_meta($post_id, 'country', $website['result']['locations'][$i]['country']); 
                                  update_post_meta($post_id, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                  if($website['result']['locations'][$i]['forwardingPhone']==''){

                                    update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['phone']);  
                                    
                                  }else{

                                    update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                  }
                                                                     
                                  update_post_meta($post_id, 'latitude', $website['result']['locations'][$i]['lat']); 
                                  update_post_meta($post_id, 'longitue', $website['result']['locations'][$i]['lng']); 
                                  update_post_meta($post_id, 'monday', $website['result']['locations'][$i]['monday']); 
                                  update_post_meta($post_id, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                  update_post_meta($post_id, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                  update_post_meta($post_id, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                  update_post_meta($post_id, 'friday', $website['result']['locations'][$i]['friday']); 
                                  update_post_meta($post_id, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                  update_post_meta($post_id, 'sunday', $website['result']['locations'][$i]['sunday']); 
                                  
                          }else{

                                    update_post_meta($found_post, 'address', $website['result']['locations'][$i]['address']); 
                                    update_post_meta($found_post, 'city', $website['result']['locations'][$i]['city']); 
                                    update_post_meta($found_post, 'state', $website['result']['locations'][$i]['state']); 
                                    update_post_meta($found_post, 'country', $website['result']['locations'][$i]['country']); 
                                    update_post_meta($found_post, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                    if($website['result']['locations'][$i]['forwardingPhone']==''){

                                    update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['phone']);  
                                    
                                    }else{

                                    update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                    }
                                                                    
                                    update_post_meta($found_post, 'latitude', $website['result']['locations'][$i]['lat']); 
                                    update_post_meta($found_post, 'longitue', $website['result']['locations'][$i]['lng']); 
                                    update_post_meta($found_post, 'monday', $website['result']['locations'][$i]['monday']); 
                                    update_post_meta($found_post, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                    update_post_meta($found_post, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                    update_post_meta($found_post, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                    update_post_meta($found_post, 'friday', $website['result']['locations'][$i]['friday']); 
                                    update_post_meta($found_post, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                    update_post_meta($found_post, 'sunday', $website['result']['locations'][$i]['sunday']); 

                          }

                  }
              
              }

          }    

}





//get ipaddress of visitor

function getUserIpAddr() {
  $ipaddress = '';
  if (isset($_SERVER['HTTP_CLIENT_IP']))
      $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
  else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
  else if(isset($_SERVER['HTTP_X_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
  else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
      $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
  else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
  else if(isset($_SERVER['HTTP_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_FORWARDED'];
  else if(isset($_SERVER['REMOTE_ADDR']))
      $ipaddress = $_SERVER['REMOTE_ADDR'];
  else
      $ipaddress = 'UNKNOWN';
  
  if ( strstr($ipaddress, ',') ) {
          $tmp = explode(',', $ipaddress,2);
          $ipaddress = trim($tmp[1]);
  }
  return $ipaddress;
}


// Custom function for lat and long

function get_storelisting() {
  
  global $wpdb;
  $content="";
  
  $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';

  $response = wp_remote_post( $urllog, array(
      'method' => 'GET',
      'timeout' => 45,
      'redirection' => 5,
      'httpversion' => '1.0',
      'headers' => array('Content-Type' => 'application/json'),         
      'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
      'blocking' => true,               
      'cookies' => array()
      )
  );
      
      $rawdata = json_decode($response['body'], true);
      $userdata = $rawdata['response'];

      $autolat = $userdata['pulseplus-latitude'];
      $autolng = $userdata['pulseplus-longitude'];
      if(isset($_COOKIE['preferred_store'])){

          $store_location = $_COOKIE['preferred_store'];
      }else{

          $store_location = '';
         
        }      
      
      $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
              ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
              cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
              sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
              INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
              INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
              WHERE posts.post_type = 'store-locations' 
              AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";

           

      $storeposts = $wpdb->get_results($sql);
      $storeposts_array = json_decode(json_encode($storeposts), true);      

     
      if($store_location ==''){
              
          $store_location = $storeposts_array['0']['ID'];
          $store_distance = round($storeposts_array['0']['distance'],1);
      }else{

      
          $key = array_search($store_location, array_column($storeposts_array, 'ID'));
          $store_distance = round($storeposts_array[$key]['distance'],1);           

      }
      
    
      $content = get_the_title($store_location); 
      $phone = get_field('phone', $store_location); 


  foreach ( $storeposts as $post ) {
$content_list .= '<div class=" store_wrapper" id ="'.$post->ID.'">
                
                    <h5 class="title-prefix">'.get_the_title($post->ID).'</h5>
                    <h5 class="store-add"> '.get_field('address', $post->ID).'<br />'.get_field('city',$post->ID).', '.get_field('state',$post->ID).' '.get_field('postal_code', $post->ID).'</h5>';
                  

                    if($post->ID != '1143810'){
                       
                        if(get_field(strtolower(date("l")), $post->ID) == 'CLOSED'){

                            $content_list .= '<p class="store-hour">CLOSED TODAY</p>';   
    
                            }else{

                            $openuntil = explode("-",get_field(strtolower(date("l")), $post->ID));
                            
                            if(get_field(strtolower(date("l")), $post->ID) == ""){
                                $content_list .= '<p class="store-hour"><a href="/about/locations/'.strtolower(str_replace(' ', '-',get_field('city',$post->ID))).'" target="_self">By Appointment Only</a></p>';
                            }else{
                                $content_list .= '<p class="store-hour"><a href="/about/locations/'.strtolower(str_replace(' ', '-',get_field('city',$post->ID))).'" target="_self">OPEN UNTIL '.str_replace(' ', '', $openuntil[1]).'</a></p>';
                            }
    
                            }

                    }else{

                        $content_list .='<p class="store-hour"><span class="">Not a Showroom (office only) </p>';

                    }                  
                    $content_list .='<p class="store-phone"><a href="tel:'.get_field('phone', $post->ID).'">'.get_field('phone', $post->ID).'</a> </p>'; 
                    if($post->ID != '1143810'){
                        
                        $content_list .='<a href="javascript:void(0)" data-id="'.$post->ID.'" data-storename="'.get_the_title($post->ID).'" data-distance="'.round($post->distance,1).'" data-phone="'.get_field('phone', $post->ID).'" target="_self" class="store-cta-link choose_location">Choose Location</a>';
                        $content_list .='<a href="/about/locations/'.strtolower(str_replace(' ', '-',get_field('city',$post->ID))).'" target="_self" class="store-cta-link view_location"> View Location</a>';
                    }
                   
                    $content_list .='</div>'; 
  } 
  

  $data = array();

  $data['header']= $content;
  $data['phone']= $phone;
  $data['list']= $content_list;

  echo json_encode($data);
      wp_die();
}


add_action( 'wp_ajax_nopriv_get_storelisting', 'get_storelisting', '1' );
add_action( 'wp_ajax_get_storelisting', 'get_storelisting','1' );



//choose this location FUnctionality

add_action( 'wp_ajax_nopriv_choose_location', 'choose_location' );
add_action( 'wp_ajax_choose_location', 'choose_location' );

function choose_location() {     
      
      $content = get_the_title($_POST['store_id']) ;      

      $content = '<div class="store_wrapper">';
      $content .='<div class="locationWrapFlyer"><div class="contentFlyer"><p>You re Shopping</p>
      <h3 class="header_location_name">'. get_the_title($_POST['store_id']) . '</h3>
      <div class="mobile"><a href="tel:'.$_POST['phone'].'">'.$_POST['phone'].'</a></div></div><div class="dropIcon"></div></div></div>';

      echo $content;

  wp_die();
}




//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail_1' );

function wpse_100012_override_yoast_breadcrumb_trail_1( $links ) {
    global $post;

   if (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/vinyl/',
            'text' => 'Luxury Vinyl',
        );
        array_splice( $links, 2, -2, $breadcrumb );
        
    }

    return $links;
}


add_filter( 'gform_pre_render_10', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_10', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_10', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_10', 'populate_product_location_form' );

add_filter( 'gform_pre_render_14', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_14', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_14', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_14', 'populate_product_location_form' );

add_filter( 'gform_pre_render_6', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_6', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_6', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_6', 'populate_product_location_form' );

add_filter( 'gform_pre_render_19', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_19', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_19', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_19', 'populate_product_location_form' );

add_filter( 'gform_pre_render_21', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_21', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_21', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_21', 'populate_product_location_form' );


add_filter( 'gform_pre_render_18', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_18', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_18', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_18', 'populate_product_location_form' );

add_filter( 'gform_pre_render_17', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_17', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_17', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_17', 'populate_product_location_form' );

add_filter( 'gform_pre_render_16', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_16', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_16', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_16', 'populate_product_location_form' );

add_filter( 'gform_pre_render_13', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_13', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_13', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_13', 'populate_product_location_form' );

add_filter( 'gform_pre_render_3', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_3', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_3', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_3', 'populate_product_location_form' );

add_filter( 'gform_pre_render_4', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_4', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_4', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_4', 'populate_product_location_form' );

function populate_product_location_form( $form ) {

foreach ( $form['fields'] as &$field ) {

    // Only populate field ID 12
    if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-store' ) === false ) {
        continue;
    }      	

        $args = array(
            'post_type'      => 'store-locations',
            'posts_per_page' => -1,
            'post_status'    => 'publish'
        );										
      
         $locations =  get_posts( $args );

         $choices = array(); 

         foreach($locations as $location) {
            

             // $title =  get_post_meta( $location->ID, 'city', true ).', '.get_post_meta( $location->ID, 'state', true );

             $title = get_the_title($location->ID);
              
                   $choices[] = array( 'text' => $title, 'value' => $title );

          }
          wp_reset_postdata();

         // write_log($choices);

         // Set placeholder text for dropdown
         $field->placeholder = '-- Choose Location --';

         // Set choices from array of ACF values
         $field->choices = $choices;
    
}
return $form;
}